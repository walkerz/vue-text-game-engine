# Vue text game engine

## Project setup

```
npm install
```

### Compiles and hot-reloads for development

```
npm run serve
```

### Compiles and minifies for production

```
npm run build
```

### Lints and fixes files

```
npm run lint
```

### Customize configuration

See [Configuration Reference](https://cli.vuejs.org/config/).

# Todo

- Save and load
- Localization
- Change room parent key to cjildren array
- timeToExecuteSingleTime for clicker
- dynamic theme color to switch by actions
- images for locations
- avatars for dialogs
- Normal demo story
- HOLD buttons

<br>

# Rooms

```
roomKey: {
  parentKey: String, // Parent room key if exist
  title: String,
  visitText: String, Array // Text to show then location visited,
  actions: ({ state }) => { // Action buttons available in room
    // You can get triggers from state for example
    return [
      ...available_actions_here
    ]
  }
}
```

<br>

# Room Actions

```
{
  title: 'Open door', // Can be object for localization, for example { en: 'Hello', ru: 'Привет' }
  isClicker: true, // if button is clicker
  percentPerClick: 10, // click speed
  percentCooldownSpeed: 20, // cooldown speed after single click
  isCooldown: false, // if button is a cooldown button
  timeToExecute: 1000 * 0.5, // timeToExecute
  hidden: false, // hide
  disabled: false, // disabled
  disableAllActions: true, // disable all other actions while timeToExecute
  callInstantly: [{ // array of callbacks to execute instantly on click
    key: 'print',
    props: {
      text: 'Door is heavy'
    }
  }],
  callbacks: [{ // array of callbacks to execute after timeToExecute
    key: 'print',
    disabled: true // this callback will not be executed,
    props: {
      text: 'No answer..'
    },
  },
  {
    key: 'setTrigger',
    props: {
      key: 'start.door.knocked',
      value: true
    }
  }],
  children: [{ // array of children actions, will be shown after action timeToExecute
    ...
  }]
}
```

<br>

# Available callbacks

<br>

## Print message

```
{
  key: 'print',
  props: {
    text: {
      en: 'Hello world',
      ru: 'Привет мир'
    }
  }
}
```

### Available props

| key    | type           | default | description         | example                                       |
| ------ | -------------- | ------- | ------------------- | --------------------------------------------- |
| text   | String, Object | `null`  | Text of the message | `'Hello'` <hr> `{ en: 'Hello', ru: 'Привет'}` |
| status | String         | `null`  | Message color       | `'danger', 'success', 'warning'`              |

<br>

## Print dialog message

```
{
  key: 'printDialog',
  props: {
    title: {
      en: 'Angelo',
      ru: 'Анджел'
    },
    text: {
      en: 'Hello world',
      ru: 'Привет мир'
    }
  }
}
```

### Available props

| key   | type           | default | description                 | example                                        |
| ----- | -------------- | ------- | --------------------------- | ---------------------------------------------- |
| title | String, Object | `null`  | Title of the dialog message | `'Alex'` <hr> `{ en: 'Alex', ru: 'Александр'}` |
| text  | String, Object | `null`  | Text of the dialog message  | `'Hello'` <hr> `{ en: 'Hello', ru: 'Привет'}`  |
| left  | Boolean        | `false` | Text of the dialog message  |                                                |

```
printDialogSwitch ({
  key: String
});

removeActionFromDialogSwitch({
  switchKey: String,
  id: Number
});

disableAllActions();

enableAllActions();

setTrigger ({
  path: String, // path to trigger variable. Example: 'start_location.door.opened'
  value: AnyType // if function { state } variable will be in props
});

changeStatsValue({
  key: String,
  value: Number
});

addToInventory({
  key: String,
  count: Number
});

removeFromInventory({
  key: String,
  count: Number
});

runCutscene({
  key: String
});

runDialog({
  key: String
});

reduceStatValue ({
  key: String,
  value: Number
});

increaseStatValue ({
  key: String,
  value: Number
});

setColorScheme ({
  key: String // You should first define color scheme in ./src/data/colorSchemes/index.js
});

defaultColorScheme = {
  backgroundColor: "#1f2227", // Background color of the screen
}

dropColorScheme (); // Drop color scheme to default


```

## Cutscenes / Dialogs

```
cutscene_key: [
  {
    time: 1000 * 1, // time to next action
    callbacks: [ // Array of callbacks
      ...
    ]
  }
]
```

## Dialog Switch

```
dialog_key: {
  title: String, // Title after choose made,
  callbacks: [ // if no dialog switch actions left
    ...
  ],
  actions: [
    {
      // All params like in action button + id for interaction
      id: 1, // id to interact with action,
      title: String, // Button title,
      callbacks: [ // callbacks for action
        ...
      ]
    }
  ]
}
```

## Inventory: Available items

```
key: {
  title: String,
  weight: Number,
  actions: [ // actions to show on select
    ...
  ]
}
```

## Stats

```
key: {
  title: String,
  isBar: true, // Show bar or not
  value: 100, // Current value
  max: 100, // Max value
  min: 0, // Min value
  maxCallbacks: [ // Callbacks to call if value >= max
    ...
  ],
  minCallbacks: [ // Calbacks to call if value <= min
    ...
  ]
}
```
