import Vue from "vue";
import App from "./App.vue";
import Vuex from "vuex";
import ActionsBlock from "./components/ActionsBlock";
import PerfectScrollbar from "vue2-perfect-scrollbar";
import storeData from "./store/index.js";

import "vue2-perfect-scrollbar/dist/vue2-perfect-scrollbar.css";

Vue.config.productionTip = false;
Vue.component("ActionsBlock", ActionsBlock);

Vue.use(PerfectScrollbar);
Vue.use(Vuex);

Vue.directive("click-outside", {
  bind: function(el, binding, vNode) {
    // Provided expression must evaluate to a function.
    if (typeof binding.value !== "function") {
      const compName = vNode.context.name;
      let warn = `[Vue-click-outside:] provided expression '${binding.expression}' is not a function, but has to be`;
      if (compName) {
        warn += `Found in component '${compName}'`;
      }

      console.warn(warn);
    }

    // Define Handler and cache it on the element
    const bubble = binding.modifiers.bubble;
    const handler = (e) => {
      if (bubble || (!el.contains(e.target) && el !== e.target)) {
        binding.value(e);
      }
    };
    el.__vueClickOutside__ = handler;

    // Add Event Listeners
    document.addEventListener("click", handler);
  },

  unbind: function(el) {
    // Remove Event Listeners
    document.removeEventListener("click", el.__vueClickOutside__);
    el.__vueClickOutside__ = null;
  },
});

const store = new Vuex.Store(storeData);

new Vue({
  render: (h) => h(App),
  store,
}).$mount("#app");
