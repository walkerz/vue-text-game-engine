import state from './state/index.js';
import getters from './getters/index.js';
import actions from './actions/index.js';
import mutations from './mutations/index.js';
export default {
  state,
  getters,
  actions,
  mutations
}