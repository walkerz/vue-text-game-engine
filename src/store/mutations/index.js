import menu from './menu.js';
import settings from './settings.js';
import inventory from './inventory.js';
import log from './log.js';
import gameState from './gameState.js';
import triggers from './triggers.js';
import stats from './stats.js';
import dialogSwitches from './dialogSwitches.js';
import rooms from './rooms.js';

export default {
  ...menu,
  ...settings,
  ...inventory,
  ...log,
  ...gameState,
  ...triggers,
  ...stats,
  ...dialogSwitches,
  ...rooms
}