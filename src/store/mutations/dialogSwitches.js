export default {
  setDialogSwitchActions (state, {switchKey, actions}) {
    state.dialogSwitches[switchKey].actions = [...actions];
  }
}