export default {
  setInventoryItems (state, payload) {
    state.inventory.items = {...state.inventory.items, ...payload};
  },
  selectInventoryItem (state, key) {
    state.inventory.selectedItemKey = key;
  }
}