export default {
  disableAllActions(state) {
    state.gameState.disableAllActions = true;
  },
  enableAllActions(state) {
    state.gameState.disableAllActions = false;
  },
  setColorScheme(state, scheme) {
    state.gameState.colorScheme = scheme;
  },
};
