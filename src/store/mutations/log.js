export default {
  print(state, payload) {
    // We need unique ids for proper messages animations
    state.log = [{ ...payload, id: state.log.length }, ...state.log];
  },
};
