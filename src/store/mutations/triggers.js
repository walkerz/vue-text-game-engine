import { getObjectProperty } from "@/helpers/objects.js";

export default {
  setTrigger(state, { path, value }) {
    const { triggers } = state;
    const pathArray = path.split(".");
    const lastKey = pathArray[pathArray.length - 1];
    const prop = getObjectProperty(triggers, path);

    if (typeof value === 'function') {
      prop[lastKey] = value({ state });
    } else {
      prop[lastKey] = value;
    }
  },
};
