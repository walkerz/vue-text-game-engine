export default {
  changeSettingsValue(state, { block, option, value }) {
    block = state.settings[block];

    if (block && block.options) {
      option = block.options[option];

      if (option) {
        option.value = value;
      } else {
        console.warn(`Option not found.`);
      }
    } else {
      console.warn(`Settings block not found.`);
    }
  },
};
