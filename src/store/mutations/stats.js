export default {
  changeStatsValue (state, {name, value}) {
    state.stats[name].value = value;
  }
}