export default {
  openMenu (state, menuName) {
    state.menu[menuName].active = true;
  }
}