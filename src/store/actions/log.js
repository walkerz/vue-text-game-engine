import { parseText } from "@/helpers/text.js";

export default {
  PRINT({ commit }, { text, color }) {
    if (text) {
      if (Array.isArray(text) && text.length) {
        text = text[Math.floor(Math.random() * text.length)];
      }

      text = parseText(text);

      commit("print", {
        text,
        color,
      });
    }
  },
  PRINT_DIALOG({ commit }, { title, text, left }) {
    if (text) {
      if (Array.isArray(text)) {
        text = text[Math.floor(Math.random() * text.length)];
      }

      text = parseText(text);

      commit("print", {
        title,
        text,
        type: "dialog",
        left,
      });
    }
  },
};
