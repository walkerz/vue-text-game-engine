import { objectHasProperty } from "@/helpers/objects.js";

export default {
  SET_TRIGGER({ commit, dispatch, state }, { path, value }) {
    const { triggers } = state;

    if (objectHasProperty(triggers, path) === true) {
      commit("setTrigger", { path, value });
    } else {
      dispatch("PRINT", {
        text: `Не могу найти триггер ${path}`,
        color: "danger",
      });
    }
  },
};
