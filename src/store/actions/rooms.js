export default {
  SET_ROOM({ state, commit }, { key }) {
    const { currentRoomKey, rooms, settings } = state;

    if (currentRoomKey !== key) {
      const room = rooms[key];
      const visitText = room.visitText;

      if (room) {
        const title = room.title;

        commit("setRoom", { key });

        if (title) {
          let showText = true;

          try {
            showText = settings.game.options.changeLocationText.value;
          } catch (e) {
            console.log(e);
          }
          if (showText) {
            let text = `Я на локации ${title}`;

            if (visitText) {
              if (Array.isArray(visitText) && visitText.length) {
                text = visitText[Math.floor(Math.random() * visitText.length)];
              } else if (typeof visitText === "string") {
                text = visitText;
              }
            }
            commit("print", { text });
          }
        }
      } else {
        commit("print", {
          text: `Не могу найти комнату ${key}`,
          color: "danger",
        });
      }
    }
  },
};
