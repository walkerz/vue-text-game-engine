export default {
  OPEN_MENU (context, menuName) {
    context.commit('openMenu', menuName);
  }
}