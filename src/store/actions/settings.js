export default {
  CHANGE_SETTINGS_VALUE (context, {block, option, value}) {
    context.commit('changeSettingsValue', {block, option, value});
  }
}