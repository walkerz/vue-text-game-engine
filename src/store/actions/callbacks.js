export default {
  INIT_CALLBACKS(context, { callbacks, roomKey }) {
    const allCallbacks = context.state.callbacks;
    const roomCallbacks = roomKey ? allCallbacks[roomKey] : null;

    if (Array.isArray(callbacks) === true) {
      callbacks.forEach((item) => {
        const key = item.key;
        const props = item.props;
        const disabled = item.disabled;

        if (!disabled) {
          let callback = roomCallbacks ? roomCallbacks[key] : null;

          if (callback) {
            callback.call(context, props);
          } else {
            callback = allCallbacks.common[key];

            if (callback) {
              callback.call(context, props);
            }
          }
          if (!callback) {
            context.dispatch("PRINT", {
              text: `Не могу найти колбэк ${key}`,
              color: "danger",
            });
          }
        }
      });
    } else {
      context.dispatch("PRINT", {
        text: `Game Engine: Callbacks variable is not an Array.`,
        color: "danger",
      });
    }
  },
  CALLBACK_PRINT({ dispatch }, payload) {
    dispatch("PRINT", payload);
  },
  CALLBACK_PRINT_DIALOG({ dispatch }, payload) {
    dispatch("PRINT_DIALOG", payload);
  },
  CALLBACK_PRINT_DIALOG_SWITCH({ dispatch }, payload) {
    dispatch("PRINT_DIALOG_SWITCH", payload);
  },
  CALLBACK_SET_TRIGGER({ dispatch }, payload) {
    dispatch("SET_TRIGGER", payload);
  },
  CALLBACK_DISABLE_ALL_ACTIONS({ dispatch }) {
    dispatch("DISABLE_ALL_ACTIONS");
  },
  CALLBACK_ENABLE_ALL_ACTIONS({ dispatch }) {
    dispatch("ENABLE_ALL_ACTIONS");
  },
  CALLBACK_CHANGE_STATS_VALUE({ dispatch }, payload) {
    dispatch("CHANGE_STATS_VALUE", payload);
  },
  CALLBACK_ADD_TO_INVENTORY({ dispatch }, payload) {
    dispatch("ADD_TO_INVENTORY", payload);
  },
  CALLBACK_REMOVE_FROM_INVENTORY({ dispatch }, payload) {
    dispatch("REMOVE_FROM_INVENTORY", payload);
  },
  CALLBACK_RUN_CUTSCENE({ dispatch }, payload) {
    dispatch("RUN_CUTSCENE", payload);
  },
  CALLBACK_RUN_DIALOG({ dispatch }, payload) {
    dispatch("RUN_CUTSCENE", payload);
  },
  CALLBACK_REMOVE_ACTION_FROM_DIALOG_SWITCH({ dispatch }, payload) {
    dispatch("REMOVE_ACTION_FROM_DIALOG_SWITCH", payload);
  },
  CALLBACK_SET_COLOR_SCHEME({ dispatch }, payload) {
    dispatch("SET_COLOR_SCHEME", payload);
  },
  CALLBACK_DROP_COLOR_SCHEME({ dispatch }) {
    dispatch("DROP_COLOR_SCHEME");
  },
};
