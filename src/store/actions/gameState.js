import colorSchemes from "@/data/colorSchemes";

const DEFAULT_COLOR_SCHEME = {
  backgroundColor: "#1f2227",
};

export default {
  DISABLE_ALL_ACTIONS({ commit }) {
    commit("disableAllActions");
  },
  ENABLE_ALL_ACTIONS({ commit }) {
    commit("enableAllActions");
  },
  SET_COLOR_SCHEME({ commit }, { key }) {
    const colorScheme = colorSchemes[key];

    commit("setColorScheme", { ...DEFAULT_COLOR_SCHEME, ...colorScheme });
  },
  DROP_COLOR_SCHEME({ commit }) {
    commit("setColorScheme", DEFAULT_COLOR_SCHEME);
  },
};
