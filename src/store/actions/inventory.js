export default {
  ADD_TO_INVENTORY({ state, commit, dispatch }, { key, count }) {
    count = count || 1;
    if (key && typeof key === "string") {
      let inventoryItems = state.inventory.items;
      const availableItems = state.inventory.availableItems;

      if (availableItems[key]) {
        let itemInInventory = inventoryItems[key];

        if (itemInInventory) {
          itemInInventory.count += count;
          commit("setInventoryItems", inventoryItems);
        } else {
          inventoryItems[key] = {
            count,
          };
          commit("setInventoryItems", inventoryItems);
        }
      } else {
        dispatch("PRINT", {
          text: `Не могу найти доступный объект ${key}`,
          color: "danger",
        });
      }
    } else {
      dispatch("PRINT", {
        text: "Не передан ключ объекта",
        color: "danger",
      });
    }
  },
  REMOVE_FROM_INVENTORY(
    { state, commit, dispatch },
    { key, count, successCallbacks, errorCallbacks, emptyCallbacks }
  ) {
    count = count || 1;
    errorCallbacks = errorCallbacks || [
      {
        name: "print",
        params: [
          {
            text: "Не хватает",
          },
        ],
      },
    ];
    emptyCallbacks = emptyCallbacks || [
      {
        name: "print",
        params: [
          {
            text: "Нет в инвентаре",
          },
        ],
      },
    ];

    if (key && typeof key === "string") {
      let inventoryItems = state.inventory.items;
      const availableItems = state.inventory.availableItems;

      if (availableItems[key]) {
        let itemInInventory = inventoryItems[key];
        if (itemInInventory) {
          const diff = itemInInventory.count - count;

          if (diff < 0) {
            if (errorCallbacks && Array.isArray(errorCallbacks)) {
              dispatch("INIT_CALLBACKS", { callbacks: errorCallbacks });
            }
          } else {
            itemInInventory.count = diff;

            if (diff === 0) {
              delete inventoryItems[key];
              dispatch("SELECT_INVENTORY_ITEM", null);
            }

            if (successCallbacks && Array.isArray(successCallbacks)) {
              dispatch("INIT_CALLBACKS", { callbacks: successCallbacks });
            }
          }

          commit("setInventoryItems", inventoryItems);
        } else {
          if (emptyCallbacks && Array.isArray(emptyCallbacks)) {
            dispatch("INIT_CALLBACKS", { callbacks: emptyCallbacks });
          }
        }
      } else {
        dispatch("PRINT", {
          text: `Не могу найти доступный объект ${key}`,
          color: "danger",
        });
      }
    } else {
      dispatch("PRINT", {
        text: "Не передан ключ объекта",
        color: "danger",
      });
    }
  },
  SELECT_INVENTORY_ITEM({ commit }, key) {
    if (key === null || (key && typeof key === "string")) {
      commit("selectInventoryItem", key);
    }
  },
};
