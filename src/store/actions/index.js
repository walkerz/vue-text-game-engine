import menu from './menu.js';
import settings from './settings.js';
import inventory from './inventory.js';
import callbacks from './callbacks.js';
import log from './log.js';
import gameState from './gameState.js';
import triggers from './triggers.js';
import stats from './stats.js';
import cutscenes from './cutscenes.js';
import dialogSwitches from './dialogSwitches.js';
import rooms from './rooms.js';

export default {
  ...menu,
  ...settings,
  ...inventory,
  ...callbacks,
  ...log,
  ...gameState,
  ...triggers,
  ...stats,
  ...cutscenes,
  ...dialogSwitches,
  ...rooms
}