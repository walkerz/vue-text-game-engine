export default {
  RUN_CUTSCENE({ state, dispatch }, key) {
    const speed = state.settings.game.options.cutscenesSpeed.value;
    const availableCutscenes = state.cutscenes;

    const timer = (time) => {
      return new Promise((resolve) => {
        let timeout = setTimeout(() => {
          resolve();
        }, time);

        const preventTimeout = () => {
          clearTimeout(timeout);
          resolve();
          document.removeEventListener("mousedown", preventTimeout);
        };

        document.addEventListener("mousedown", preventTimeout);
      });
    };

    async function load(cutscene) {
      for (var i = 0; i < cutscene.length; i++) {
        const block = cutscene[i];
        const callbacks = block.callbacks;
        let time = block.time || 0;

        if (callbacks && Array.isArray(callbacks)) {
          dispatch("INIT_CALLBACKS", { callbacks });

          /*
          Time based on settings cutscenes speed for
          PRINT and PRINT_DIALOG single callbacks
          */
          if (!time && callbacks.length === 1) {
            const callback = callbacks[0];
            const callbackProps = callback.props;

            if (
              (callback.key === "print" || callback.key === "printDialog") &&
              callbackProps &&
              Array.isArray(callbackProps) &&
              callbackProps.length &&
              typeof callbackProps[0] === "object" &&
              callbackProps[0].text
            ) {
              const text = callbackProps[0].text;

              time = (text.length * 60 * 1000) / speed;
            }
          }
        }
        await timer(time);
      }
    }

    if (key) {
      const cutscene = availableCutscenes[key];

      if (cutscene !== undefined) {
        load(cutscene);
      } else {
        dispatch("PRINT", {
          text: `Game Engine: Can't find cutscene ${key}.`,
          color: "danger",
        });
      }
    } else {
      dispatch("PRINT", {
        text: `Game Engine: No cutscene key.`,
        color: "danger",
      });
    }
  },
};
