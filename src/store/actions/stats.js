export default {
  CHANGE_STATS_VALUE ({ commit, state, dispatch }, {name, value}) {
    if (name && typeof value === 'number') {
      const stats = state.stats;

      if (name in stats) {

        const item = stats[name];
        const max = item.max;
        const min = item.min;

        value += item.value;

        if (max && value >= max) {
          value = max;
        }

        if ((min || min === 0) && value <= min) {
          value = min;
        }
        
        commit('changeStatsValue', {name, value});

        if (item.maxCallbacks && max && value >= max) {
          dispatch('INIT_CALLBACKS', {
            callbacks: item.maxCallbacks
          });
        }

        if (item.minCallbacks && (min || min === 0) && value <= min) {
          dispatch('INIT_CALLBACKS', {
            callbacks: item.minCallbacks
          });
        }

      } else {
        dispatch('PRINT', {
          text: `Не могу найти статы ${name}`,
          color: 'danger'
        });
      }
    }
  }
}