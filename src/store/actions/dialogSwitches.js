export default {
  PRINT_DIALOG_SWITCH({ commit, dispatch, state }, { key }) {
    if (key) {
      const availableSwitces = state.dialogSwitches;
      const currentSwitch = availableSwitces[key];

      if (currentSwitch) {
        const actions = currentSwitch.actions;
        if (actions && Array.isArray(actions) && actions.length) {
          commit("print", {
            type: "dialogSwitch",
            title: currentSwitch.title,
            actions: currentSwitch.actions,
            left: currentSwitch.left,
          });
        } else {
          const callbacks = currentSwitch.callbacks;
          if (callbacks && Array.isArray(callbacks) && callbacks.length) {
            dispatch("INIT_CALLBACKS", { callbacks });
          }
        }
      } else {
        dispatch("print", {
          text: `Не могу найти ключ перелючателя диалога ${key}`,
          color: "danger",
        });
      }
    } else {
      dispatch("print", {
        text: "Не передан ключ перелючателя диалога",
        color: "danger",
      });
    }
  },
  REMOVE_ACTION_FROM_DIALOG_SWITCH(
    { state, dispatch, commit },
    { switchKey, id }
  ) {
    let availabeSwitches = state.dialogSwitches;
    if (availabeSwitches) {
      let selectedSwitch = availabeSwitches[switchKey];
      if (selectedSwitch) {
        let actions = selectedSwitch.actions;
        if (actions && Array.isArray(actions) && actions.length) {
          if (id) {
            actions = actions.filter((item) => item.id !== id);
            commit("setDialogSwitchActions", {
              switchKey,
              actions,
            });
          } else {
            dispatch("PRINT", {
              text: `Не передан id действия диалога ${switchKey}`,
              color: "danger",
            });
          }
        }
      } else {
        dispatch("PRINT", {
          text: `Не могу найти диалог ${switchKey}`,
          color: "danger",
        });
      }
    } else {
      dispatch("PRINT", {
        text: "Нет доступных диалогов",
        color: "danger",
      });
    }
  },
};
