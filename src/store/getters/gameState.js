export default {
  gameState: state => {
    return state.gameState;
  },
  colorScheme: state => {
    return state.gameState.colorScheme;
  }
}