export default {
  inventory: state => {
    return state.inventory;
  },
  inventoryCurrentWeight: state => {
    const inventoryItems = state.inventory.items;
    const inventoryKeys = Object.keys(inventoryItems);
    const availableItems = state.inventory.availableItems;
    let totalWeight = 0;

    for (const key of inventoryKeys) {
      const item = availableItems[key];
      const count = inventoryItems[key].count || 0;
      if (item) {
        totalWeight += count * (item.weight || 0); 
      }
    }

    return new Number(totalWeight.toFixed(2));
  },
  inventoryItems: state => {
    const inventoryItems = state.inventory.items;
    const availableItems = state.inventory.availableItems;
    const inventoryKeys = Object.keys(inventoryItems);
    let items = {}

    for (const key of inventoryKeys) {
      const item = availableItems[key];
      const count = inventoryItems[key].count || 0;
      items[key] = {
        ...item,
        count
      }
    }

    return items;
  },
  selectedInventoryItemKey: state => {
    return state.inventory.selectedItemKey
  },
  selectedInventoryItem: state => {
    const selectedId = state.inventory.selectedItemId;
    const inventoryItems = state.inventory.items;
    const availableItems = state.inventory.availableItems;

    if (selectedId) {
      const itemInInventory = inventoryItems.find(item => item.id === selectedId);
      if (itemInInventory) {
        const item = availableItems.find(item => item.id === selectedId);
        if (item) {
          return {
            ...item,
            count: itemInInventory.count
          }
        }
      }
    } else {
      return null;
    }
  }
}