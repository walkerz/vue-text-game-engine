export default {
  rooms: (state) => state.rooms,
  currentRoomKey: (state) => state.currentRoomKey,
  currentRoom: (state) => state.rooms[state.currentRoomKey],
  currentRoomActions: (state) =>
    state.rooms[state.currentRoomKey].actions
      ? state.rooms[state.currentRoomKey].actions({ state })
      : [],
};
