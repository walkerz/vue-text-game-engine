import settings from './settings.js';
import menu from './menu.js';
import inventory from './inventory.js';
import log from './log.js';
import gameState from './gameState.js';
import triggers from './triggers.js';
import stats from './stats.js';
import rooms from './rooms.js';

export default {
  ...settings,
  ...menu,
  ...inventory,
  ...log,
  ...gameState,
  ...triggers,
  ...stats,
  ...rooms
}