export default {
  settings: state => {
    return state.settings;
  },
  gameSettings: state => {
    return state.settings.game.options;
  },
  videoSettings: state => {
    return state.settings.video.options;
  },
  language: state => {
    return state.settings.game.options.language.value
  }
}