import log from "./log";
import gameState from "./gameState";
import callbacks from "./callbacks";
import settings from "./settings";
import menu from "./menu";
import data from "../../data";

export default {
  ...log,
  ...settings,
  ...menu,
  ...callbacks,
  ...gameState,
  ...data,
};
