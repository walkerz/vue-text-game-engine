export default {
  settings: {
    game: {
      title: 'Game',
      options: {
        language: {
          title: 'Language',
          type: 'selectbox',
          value: 'en',
          values: [
            'en',
            'ru'
          ]
        },
        changeLocationText: {
          title: 'Show location visit text',
          type: 'checkbox',
          value: true
        },
        cutscenesSpeed: {
          title: 'Text speed',
          type: 'selectbox',
          value: 1200,
          values: [
            900,
            1200,
            1500,
            1800,
            3000,
            5000,
            10000
          ]
        },
        logDirection: {
          title: 'Text direction',
          type: 'selectbox',
          value: 'DESC',
          values: [
            'DESC',
            'ASC'
          ]
        }
      }
    },
    video: {
      title: 'Video',
      options: {
        font: {
          title: 'Font',
          type: 'selectbox',
          value: 'retro',
          values: [
            'retro',
            'sans-serif',
            'serif'
          ]
        },
        fontSize: {
          title: 'Font size',
          type: 'number',
          value: 12
        },
        animationLog: {
          title: 'Show text animation',
          type: 'checkbox',
          value: true
        }
      }
    }
  }
}