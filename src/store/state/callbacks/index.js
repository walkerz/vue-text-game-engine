import userCallbacks from '../../../data/callbacks';

export default {
  callbacks: {
    ...userCallbacks, 
    common: {
      print(payload) {
        this.dispatch("CALLBACK_PRINT", payload);
      },
      printDialog(payload) {
        this.dispatch("CALLBACK_PRINT_DIALOG", payload);
      },
      printDialogSwitch(payload) {
        this.dispatch("CALLBACK_PRINT_DIALOG_SWITCH", payload);
      },
      removeActionFromDialogSwitch(payload) {
        this.dispatch("CALLBACK_REMOVE_ACTION_FROM_DIALOG_SWITCH", payload);
      },
      disableAllActions() {
        this.dispatch("CALLBACK_DISABLE_ALL_ACTIONS");
      },
      enableAllActions() {
        this.dispatch("CALLBACK_ENABLE_ALL_ACTIONS");
      },
      setTrigger(payload) {
        this.dispatch("CALLBACK_SET_TRIGGER", payload);
      },
      changeStatsValue(payload) {
        this.dispatch("CALLBACK_CHANGE_STATS_VALUE", payload);
      },
      addToInventory(payload) {
        this.dispatch("CALLBACK_ADD_TO_INVENTORY", payload);
      },
      removeFromInventory(payload) {
        this.dispatch("CALLBACK_REMOVE_FROM_INVENTORY", payload);
      },
      runCutscene(payload) {
        this.dispatch("CALLBACK_RUN_CUTSCENES", payload);
      },
      runDialog(payload) {
        this.dispatch("CALLBACK_RUN_DIALOG", payload);
      },
      setColorScheme(payload) {
        this.dispatch("CALLBACK_SET_COLOR_SCHEME", payload);
      },
      dropColorScheme() {
        this.dispatch("CALLBACK_DROP_COLOR_SCHEME");
      },
    },
  },
};
