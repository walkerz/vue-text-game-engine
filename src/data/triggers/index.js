import start from './start';

export default {
  triggers: {
    ...start
  }
}