export default {
  stats: {
    hp: {
      title: "HP",
      isBar: true,
      value: 100,
      max: 100,
      min: 0,
      maxCallbacks: [
        {
          name: "print",
          params: [
            {
              text: "I feel great",
              type: "success",
            },
          ],
        },
      ],
      minCallbacks: [
        {
          name: "print",
          params: [
            {
              text: "You are dead.",
              type: "danger",
            },
          ],
        },
      ],
    },
    intellect: {
      title: "INT",
      value: 0,
      min: 0,
    },
    strength: {
      title: "STR",
      value: 0,
      min: 0,
    },
  },
};
