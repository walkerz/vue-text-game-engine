import rooms from './rooms';
import cutscenes from './cutscenes';
import inventory from './inventory';
import stats from './stats';
import triggers from './triggers';

export default {
  ...rooms,
  ...cutscenes,
  ...inventory,
  ...stats,
  ...triggers
}