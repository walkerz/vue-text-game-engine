export default {
  title: {
    en: "Start location",
    ru: "Стартовая локация",
  },
  actions: ({ state }) => {
    const { triggers } = state;

    return [
      {
        title: {
          en: "Regular action",
          ru: "Обычное действие",
        },
        timeToExecute: 1000 * 0.5,
        callbacks: [
          {
            key: "print",
            props: {
              text: "Regular action executed after some time.",
            },
          },
        ],
      },
      {
        title: "Cooldown action",
        timeToExecute: 1000 * 0.5,
        isCooldown: true,
        callbacks: [
          {
            key: "print",
            props: {
              text:
                "Action executed after cooldown. This action is just a regular action with reversed animation.",
            },
          },
        ],
      },
      {
        title: "Clicker action",
        isClicker: true,
        percentPerClick: 34,
        percentCooldownSpeed: 34,
        callbacks: [
          {
            key: "print",
            props: {
              text: "Action executed after several clicks.",
            },
          },
        ],
      },
    ];
  },
};
