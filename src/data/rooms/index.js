import start from './start';

export default {
  currentRoomKey: "start",
  rooms: {
    start
  },
};
