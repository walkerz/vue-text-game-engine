export default {
  firewood: {
    title: "Дрова",
    weight: 1,
    actions: [
      {
        title: "Выбросить",
        timeToExecute: 1000 * 0,
        callbacks: [
          {
            name: "removeFromInventory",
            params: [
              {
                key: "firewood",
              },
            ],
          },
        ],
      },
    ],
  },
  stone: {
    title: "Камень",
    weight: 1.2,
    actions: [
      {
        title: "Выбросить",
        timeToExecute: 1000 * 0,
        callbacks: [
          {
            name: "removeFromInventory",
            params: [
              {
                key: "stone",
              },
            ],
          },
        ],
      },
    ],
  },
};
