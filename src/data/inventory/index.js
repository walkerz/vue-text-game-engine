import availableItems from "./availableItems";

export default {
  inventory: {
    enabled: true,
    maxWeight: 16,
    selectedItemKey: null,
    items: {},
    availableItems,
  },
};
