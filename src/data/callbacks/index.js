export default {
  // We can set special callbacks for rooms by their key
  start: {
    special_callback() {
      this.dispatch("CALLBACK_PRINT", { text: "Special callback!" });
    },
  },
}