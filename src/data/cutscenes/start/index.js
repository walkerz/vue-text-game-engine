import { Player as PlayerClass } from "@/data/classes/Player";

const Player = new PlayerClass();

export default {
  start_cutscene: [
    {
      time: 1 * 1000, // time before next callback
      callbacks: [
        {
          key: "disableAllActions",
        },
      ],
    },
    {
      time: 1000 * 2,
      callbacks: [
        Player.print({ en: "Hello world!", ru: "Привет мир!" }),
      ],
    },
    {
      callbacks: [
        {
          key: "enableAllActions",
        },
      ],
    },
  ],
};
