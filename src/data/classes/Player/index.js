import Character from "@/data/classes/Character";

class Player extends Character {
  constructor() {
    super({
      name: {
        en: "Player",
        ru: "Персонаж",
      },
    });
  }
}

export { Player };
