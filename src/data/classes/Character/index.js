export default class Character {
  constructor({ name }) {
    this.name = name;
  }
  
  print(text) {
    return {
      key: "printDialog",
      props: {
        title: this.name,
        text,
      },
    };
  }
}