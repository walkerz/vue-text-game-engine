export default {
  methods: {
    getFixedPopupCoords({target, popup, bottomOffset, leftOffset}) {
      bottomOffset = bottomOffset || 0;
      leftOffset = leftOffset || 0;
      const box = target.getBoundingClientRect();
      const popupHeight = popup.clientHeight;
      const windowHeight = window.innerHeight + pageYOffset;
      
      let top = box.top + pageYOffset;
      let left = box.left + pageXOffset + box.width + leftOffset;

      if (top + popupHeight > windowHeight) {
        top = windowHeight - popupHeight - bottomOffset;
      }
      
      return {
        top,
        left
      };
    
    }
  }
}