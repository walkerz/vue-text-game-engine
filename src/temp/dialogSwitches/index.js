export default {
  dialogSwitches: {
    home_dialog_1_switch_1: {
      title: 'Тетч',
      callbacks: [{
        name: 'print',
        params: [{
          text: 'Диалог окончен'
        }]
      },
      {
        name: 'enableAllActions'
      }],
      actions: [
        {
          id: 1,
          title: 'Районная прописка есть?',
          callbacks: [{
            name: 'removeActionFromDialogSwitch',
            params: [{
              switchKey: 'home_dialog_1_switch_1',
              id: 1
            }]
          },
          {
            name: 'printDialog',
            params: [{
              title: 'Бабуля',
              text: 'Нет, откуда она у меня?',
              left: true
            }]
          },
          {
            name: 'printDialogSwitch',
            params: [{
              key: 'home_dialog_1_switch_1'
            }]
          }]
        },
        {
          id: 2,
          title: 'Страховка есть?',
          callbacks: [{
            name: 'removeActionFromDialogSwitch',
            params: [{
              switchKey: 'home_dialog_1_switch_1',
              id: 2
            }]
          },
          {
            name: 'printDialog',
            params: [{
              title: 'Бабуля',
              text: 'Да кто же мне ее оформит -то?',
              left: true
            }]
          },
          {
            name: 'printDialogSwitch',
            params: [{
              key: 'home_dialog_1_switch_1'
            }]
          }]
        }
      ]
    }
  }
}