export default {
  home_dialog_1: [
    {
      time: 1000 * 2,
      callbacks: [{
        name: 'printDialog',
        params: [{
          title: 'Тетч',
          text: 'Что случилось, [[color-success]]бабуль[[/color-success]]?'
        }],
      }]
    },
    {
      time: 1000 * 2,
      callbacks: [{
        name: 'printDialog',
        params: [{
          title: 'Бабуля',
          text: 'Да тут такое дело, [[color-danger]]милок...[[/color-danger]]',
          left: true
        }],
      }]
    },
    {
      time: 1000 * 0,
      callbacks: [{
        name: 'printDialogSwitch',
        params: [{
          key: 'home_dialog_1_switch_1'
        }],
      }]
    }
  ]
}