export default {
  neirotech_cutscene_1: [
    {
      time: 1 * 1000,
      callbacks: [{
        name: 'disableAllActions'
      }]
    },
    {
      callbacks: [{
        name: 'print',
        params: [{
          text: 'Протез из холодного и немного ржавого метала лежал передо мной на столе.'
        }]
      }]
    },
    {
      callbacks: [{
        name: 'print',
        params: [{
          text: 'Пришлось снять его полностью, потому что хозяин был уже немного пьян и чрезмерно болтлив.'
        }]
      }]
    },
    {
      callbacks: [{
        name: 'print',
        params: [{
          text: 'Пусть подождет в приемной, заодно немного протрезвеет.'
        }]
      }]
    },
    {
      callbacks: [{
        name: 'print',
        params: [{
          text: 'Я подключил шлейф напрямую к плате протеза и пододвинулся ближе к терминалу.'
        }]
      }]
    },
    /*{
      time: 1000 * 7,
      callbacks: [{
        name: 'print',
        params: [{
					text: 'Как только пациент пройдет через рамку на выходе из клиники, с его счета будут списаны коины за процедуру, а я получу положенные мне десять процентов.'
        }]
      }]
    },
    {
      time: 1000 * 3,
      callbacks: [{
        name: 'print',
        params: [{
          text: 'Я подключил шлейф напрямую к плате протеза и пододвинулся ближе к терминалу.'
        }],
      }]
    },
    {
      time: 1000 * 1,
      callbacks: [{
        name: 'print',
        params: [{
          text: 'Задерживаться сегодня я не планировал, но и лишние коины не помешаю.'
        }],
      }]
		},
		*/
    {
      callbacks: [{
        name: 'enableAllActions'
      }]
    },
  ],
  neirotech_cutscene_2: [
    {
      time: 1000 * 3,
      callbacks: [{
        name: 'print',
        params: [{
          text: '- nanotech send /documents/proto_1.0.2/recover.nt com3 -R -f'
        }],
      }]
    },
    {
      time: 1000 * 2,
      callbacks: [{
        name: 'print',
        params: [{
          text: '241 килобайт данных устремились по проводам от терминала в программатор и дальше в микроконтроллер протеза.'
        }],
      }]
    }
  ]
}