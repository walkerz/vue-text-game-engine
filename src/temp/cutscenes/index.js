import cutscenes from './cutscenes/index.js';
import dialogs from './dialogs/index.js';

export default {
  cutscenes: {
    ...cutscenes,
    ...dialogs
  }
}