export function parseText (text) {
  if (text && typeof text === 'string') {
    const replaces = {
      '[[strong]]': '<span class="bold">',
      '[[color-success]]': '<span class="color-success">',
      '[[color-warning]]': '<span class="color-warning">',
      '[[color-danger]]': '<span class="color-danger">',
      '[[color-grey]]': '<span class="color-grey">',
      '[[color-info]]': '<span class="color-info">'
    }
    const keys = Object.keys(replaces);
    const keyRegexp = /\[\[\S*\]\]/;
    if (keyRegexp.test(text)) {
      if (keys.length) {
        for (let key of keys) {
          const value = replaces[key];
          const closeKey = key.replace('[[', '[[/');
          const regexp = /^\S+/;
          const tags = value.match(regexp);
  
          if (tags && Array.isArray(tags) && tags.length) {
            let tag = tags[0];
            const tagRegexp = /^<\S+/;
            if (tag && tagRegexp.test(tag)) {
              let closeTag = tag.replace('<', '</') + '>';
  
              text = text.replaceAll(key, value);
              text = text.replaceAll(closeKey, closeTag);
            }
          }
        }
      }
    }
  }
  return text;
}