export const objectHasProperty = (obj, path) => {
  const pathArray = path.split(".");
  let hasProperty = true;

  for (let i = 0; i < pathArray.length; i++) {
    const key = pathArray[i];

    if (Object.prototype.hasOwnProperty.call(obj, key) === false) {
      hasProperty = false;
    } else {
      obj = obj[pathArray[i]];
    }
  }

  return hasProperty;
};

export const getObjectPropertyValue = (obj, path) => {
  const pathArray = path.split(".");

  for (let i = 0; i < pathArray.length; i++) {
    const key = pathArray[i];

    obj = obj[key];
  }

  return obj;
};

export const getObjectProperty = (obj, path) => {
  const pathArray = path.split(".");

  for (let i = 0; i < pathArray.length - 1; i++) {
    const key = pathArray[i];

    obj = obj[key];
  }

  return obj;
};
